
#include "MQTT.h"



int UTF_encode(uint8_t* input,uint8_t*output)
{
    int len=0;
	  int i=0;
    output[len]=strlen((char*)input)>>8;
		output[++len]=strlen((char*)input) & 0xFF;
		for(i=0;i<strlen((char*)input);i++)
   {
      output[++len]=input[i];
   }
	 return (++len);
}
int connect_packet(uint8_t* connect_buffer,char* protocol_name,uint8_t protocol_level, char *ClientIdentifier, char UserNameFlag, char PasswordFlag, char *UserName, char *Password, char CleanSession, char WillFlag, char WillQoS, char WillRetain, char *WillTopic, char *WillMessage,uint16_t keepalive )
{
	 int i=0;
   int len_connect=0;
   uint8_t output_buff[50];
	 int len_output=0;
   connect_buffer[len_connect]=CONNECT*16;
	 len_connect=len_connect+1;
	 memset(output_buff,0,50);
	 len_output=UTF_encode((uint8_t*)protocol_name,output_buff);
	 for(i=0;i<len_output;i++)
   {
      connect_buffer[++len_connect]=output_buff[i];
   }
	 connect_buffer[++len_connect]= protocol_level;
	 uint8_t connect_flag;
	 connect_flag=UserNameFlag * User_Name_Flag_Mask + PasswordFlag * Password_Flag_Mask + WillRetain * Will_Retain_Mask + WillQoS * Will_QoS_Scale + WillFlag * Will_Flag_Mask + CleanSession * Clean_Session_Mask;
	 connect_buffer[++len_connect]=connect_flag;
	 if(keepalive >= 256)
	 {
	   connect_buffer[++len_connect]=keepalive>>8;
		 connect_buffer[++len_connect]=keepalive&0xFF;
	 }
	 else if(keepalive < 256)
	 {
	   connect_buffer[++len_connect]=0;
		 connect_buffer[++len_connect]=keepalive;
	 }
	 len_output=0;
	 memset(output_buff,0,50);
	 len_output=UTF_encode((uint8_t*)ClientIdentifier,output_buff);
	 for(i=0;i<len_output;i++)
   {
      connect_buffer[++len_connect]=output_buff[i];
   }
	 if (WillFlag != 0)
    {
       len_output=0;
	     memset(output_buff,0,50);
	     len_output=UTF_encode((uint8_t*)WillTopic,output_buff);
	     for(i=0;i<len_output;i++)
       {
          connect_buffer[++len_connect]=output_buff[i];
       }
			  len_output=0;
	     memset(output_buff,0,50);
	     len_output=UTF_encode((uint8_t*)WillMessage,output_buff);
	     for(i=0;i<len_output;i++)
       {
          connect_buffer[++len_connect]=output_buff[i];
       }
    }
  if (UserNameFlag != 0)
  {
	    len_output=0;
	     memset(output_buff,0,50);
	     len_output=UTF_encode((uint8_t*)UserName,output_buff);
	     for(i=0;i<len_output;i++)
       {
          connect_buffer[++len_connect]=output_buff[i];
       }
    if (PasswordFlag != 0)
    {
		   len_output=0;
	     memset(output_buff,0,50);
	     len_output=UTF_encode((uint8_t*)Password,output_buff);
	     for(i=0;i<len_output;i++)
       {
          connect_buffer[++len_connect]=output_buff[i];
       }
    }
	  
  }
  connect_buffer[1]=len_connect-1;	
	
	return (len_connect+1);
}

int publish_packet(uint8_t* publish_buffer,char Qos, char RETAIN, unsigned int MessageID, char *Topic, char *Message)
{
	int i=0;
  int len_publish=0;
  publish_buffer[len_publish]=PUBLISH * 16 +  Qos * QoS_Scale + RETAIN;
	len_publish=len_publish+1;
	uint8_t output_buff[50];
  int len_output=0;
	memset(output_buff,0,50);
	len_output=UTF_encode((uint8_t*)Topic,output_buff);
	for(i=0;i<len_output;i++)
  {
    publish_buffer[++len_publish]=output_buff[i];
  }
	if(Qos>0)
	{
	   publish_buffer[++len_publish]=MessageID>>8;
		 publish_buffer[++len_publish]=MessageID & 0xFF;
	}
	for(i=0;i<strlen(Message);i++)
  {
    publish_buffer[++len_publish]=Message[i];
  }
	publish_buffer[1]=len_publish-1;	
	
	return (len_publish+1);
}

int subscribe_packet(uint8_t* subscribe_buffer, unsigned int MessageID, char *SubTopic, char SubQoS)
{
	int i=0;
  int len_subscribe=0;
	subscribe_buffer[len_subscribe]=(SUBSCRIBE * 16) + (1 * QoS_Scale);
	len_subscribe=len_subscribe+1;
	subscribe_buffer[++len_subscribe]=MessageID>>8;
  subscribe_buffer[++len_subscribe]=MessageID & 0xFF;
	uint8_t output_buff[50];
  int len_output=0;
	memset(output_buff,0,50);
	len_output=UTF_encode((uint8_t*)SubTopic,output_buff);
	for(i=0;i<len_output;i++)
  {
   subscribe_buffer[++len_subscribe]=output_buff[i];
  }
	subscribe_buffer[++len_subscribe]=SubQoS;
	subscribe_buffer[1]=len_subscribe-1;	
	
	return (len_subscribe+1);
}

