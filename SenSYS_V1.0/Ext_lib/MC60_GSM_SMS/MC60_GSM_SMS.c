#include "MC60_GSM_SMS.h"
#include "stdio.h"
/************global variables*****************/
char rx_send_msg[50];
char rx_new_msg_indication[100];
char rx_set_sms_text_mode[50];
char rx_delete_all_messages[100];
char end_test[2];;
char rx_set_character_set_to_gsm[50];
/********************************************/
void send_AT_command(uint8_t* command,int length)
{
   HAL_UART_Transmit(&huart1,(uint8_t*)command,length,100);
}
/**
  * @brief  Send SMS 
  * @param  Mobile number and the message to be sent
  * @retval None
  */
void send_msg(char* number, char* message)
{
	memset(rx_send_msg,0,50);
	char tx_send_msg[100];
	end_test[0]=0x1A;
	sprintf(tx_send_msg,"AT+CMGS=\"%s\"\r\n",number);
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_send_msg,strlen(tx_send_msg),100);
	HAL_Delay(500);
	HAL_UART_Transmit(&huart1,(uint8_t*)message,strlen(message),100);
	HAL_UART_Transmit(&huart1,(uint8_t*)end_test,1,100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_send_msg,50, 100);  // Receiving data via usart
	
	
}
/**
  * @brief  delete all messages in the SIM
  * @param  None
  * @retval None
  */

void delete_all_messages(void)
{
	memset(rx_delete_all_messages,0,100);
	char tx_delete_all_messages[100]="AT+CMGD=1,4\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_delete_all_messages,strlen(tx_delete_all_messages),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_delete_all_messages,50, 500);  // Receiving data via usart
	
}
/**
  * @brief  Read all the messages
  * @param  message that has been received 
  * @retval None
  */
void list_messages(char* rx_list_messages)
{
	HAL_Delay(1000);
	memset(rx_list_messages,0,500);
	char tx_list_messages[100]="AT+CMGL=\"REC UNREAD\"\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_list_messages,strlen(tx_list_messages),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_list_messages,500, 4000);  // Receiving data via usart
}
/**
  * @brief  Send AT command to activate new message indication
  * @param  None
  * @retval None
  */
void new_msg_indication()
{
	memset(rx_new_msg_indication,0,100);
	char tx_new_msg_indication[100]="AT+CNMI=2,1\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_new_msg_indication,strlen(tx_new_msg_indication),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_new_msg_indication,100, 100);  // Receiving data via usart
}
/**
  * @brief  To set SMS in TEXT Mode
  * @param  None
  * @retval None
  */
void set_sms_text_mode()
{
	memset(rx_set_sms_text_mode,0,50);
	char tx_set_sms_text_mode[100]="AT+CMGF=1\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_set_sms_text_mode,strlen(tx_set_sms_text_mode),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_set_sms_text_mode,100, 100);  // Receiving data via usart
}
/**
  * @brief  To set character set in SMS in GSM Mode
  * @param  None
  * @retval None
  */
void set_character_set_to_gsm()
{
	memset(rx_set_character_set_to_gsm,0,50);
	char tx_set_character_set_to_gsm[100]="AT+CSCS=\"GSM\"\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_set_character_set_to_gsm,strlen(tx_set_character_set_to_gsm),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_set_character_set_to_gsm,100, 100);  // Receiving data via usart
}




