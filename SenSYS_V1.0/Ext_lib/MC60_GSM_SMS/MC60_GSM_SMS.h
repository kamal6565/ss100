#ifndef __MC60_GSM_SMS_H
#define __MC60_GSM_SMS_H

#include "string.h"
#include "stdbool.h"
#include "stm32f1xx_hal.h"

#define  END_MSG     0x1A

void send_AT_command(uint8_t* command,int length);  //send AT command
void send_msg(char* number, char* message); //Send SMS
void new_msg_indication(void);            //Send AT command to activate new message indication
void set_sms_text_mode(void);             //To set SMS in TEXT Mode
void list_messages(char* rx_list_messages); //Read all the messages
void delete_all_messages(void);            //delete all messages in the SIM
void set_character_set_to_gsm(void);      //To set character set in SMS in GSM Mode

extern UART_HandleTypeDef huart1;


#endif
