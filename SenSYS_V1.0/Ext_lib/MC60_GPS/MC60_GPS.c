#include "MC60_GPS.h"
#include "string.h"
#include "stdbool.h"
#include "stdlib.h"

/******************global variables***********************/
char rx_set_gnss_on[100];
char rx_get_rmc_gnss_information[100];
char rx_get_vtg_gnss_information[200];
char rx_extract_gsm_signal_strength[200];
char rx_imei_no[100];
char rx_read_date_time[100];
char lattitude_in_float[20];
char rx_extract_mcc_mnc_cellid_lac[200];
/*********************************************/


/**
  * @brief  To make GNSS ON
  * @param  None
  * @retval None
  */
void set_gnss_on(void)
{
	memset(rx_set_gnss_on,0,100);
	char tx_set_gnss_on[100]="AT+QGNSSC=1\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_set_gnss_on,strlen(tx_set_gnss_on),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_set_gnss_on,100, 100);  // Receiving data via usart
}

/******************global variables***********************/
char split1[100];
char split2[100];
char gps_active_void[25];
/**********************************************/
/**
  * @brief  To extract RMC information
  * @param  None
  * @retval None
  */
void get_rmc_gnss_information(GPS_GGA_GNSS_Def *pThis)
{
	memset(gps_active_void,0,sizeof(gps_active_void));
	memset(rx_get_rmc_gnss_information,0,50);
	char tx_get_rmc_gnss_information[100]="AT+QGNSSRD=\"NMEA/RMC\"\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_get_rmc_gnss_information,strlen(tx_get_rmc_gnss_information),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_get_rmc_gnss_information,100, 100);  // Receiving data via usart
	/********************split the string based on ","*********************/
	char *ptr1,*ptr2,*ptr3;
	//split( received_string, '+', &res4 );
	char delim[] = ",";

	char *ptr = strtok(rx_get_rmc_gnss_information, delim);
  ptr1=ptr;
  ptr = strtok(NULL, delim);
  ptr2=ptr;
	ptr = strtok(NULL, delim);
  ptr3=ptr;
/**************************************************************/
	strcpy(split1,ptr1);    
	strcpy(split2,ptr2);
	strcpy(gps_active_void,ptr3);
  if(strncmp(gps_active_void,"A",1)==0)
	{
		pThis->GPS_ACTIVE = true;
	}
	else
	{
		pThis->GPS_ACTIVE = true;
	}
	
}

/******************global variables***********************/
char rx_get_gga_gnss_information[500];
char lattitude_char[100];
char direction_of_lattitude[100];
char longitude_char[100];
char direction_of_longitude_char [100];
char split7[100];
char no_of_sattelites_char[100];
char lattitude_degree[5];
char lattitude_minutes[10];
float lattitude_minute_float;
int lattitude_degree_int;
char longitude_degree[5];
char longitude_minutes[10];
float longitude_minute_float;
int longitude_degree_int;
/********************************************************/
/**********************************************/
 char hour_char[10];
 char minute_char[10];
 char seconds_char[10];
 char day_char[10];
 char month_char[10];
 char year_char[10];
double latitude_check;
double longitude_check;
/**********************************************/
/**
  * @brief  To extract GGA information
  * @param  None
  * @retval None
  */
void get_gga_gnss_information(GPS_GGA_GNSS_Def *pThis)
{
	int j=0;
	memset(rx_get_gga_gnss_information,0,50);
	char tx_get_gga_gnss_information[100]="AT+QGNSSRD=\"NMEA/GGA\"\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_get_gga_gnss_information,strlen(tx_get_gga_gnss_information),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_get_gga_gnss_information,500, 100);  // Receiving data via usart
		/********************split the string based on ","*********************/  
	char *ptr1,*ptr2,*ptr3,*ptr4,*ptr5,*ptr6,*ptr7,*ptr8;
	
	char delim[] = ",";

	char *ptr = strtok(rx_get_gga_gnss_information, delim);
    ptr1=ptr;
    ptr = strtok(NULL, delim);
    ptr2=ptr;
	ptr = strtok(NULL, delim);
    ptr3=ptr;
	ptr = strtok(NULL, delim);
    ptr4=ptr;
	    ptr = strtok(NULL, delim);
    ptr5=ptr;
	ptr = strtok(NULL, delim);
    ptr6=ptr;
	ptr = strtok(NULL, delim);
    ptr7=ptr;
		ptr = strtok(NULL, delim);
  ptr8=ptr;
	strcpy(split1,ptr1);
	strcpy(split2,ptr2);
	strcpy(lattitude_char,ptr3);
	strcpy(direction_of_lattitude,ptr4);
	strcpy(longitude_char,ptr5);
	strcpy(direction_of_longitude_char ,ptr6);
	strcpy(split7,ptr7);
	strcpy(no_of_sattelites_char,ptr8);
	/*******************************************************/
	/**********extracting the string************************/
  memset(hour_char,0,sizeof(hour_char));
  memset(minute_char,0,sizeof(minute_char));
  memset(seconds_char,0,sizeof(seconds_char));
	for( j=0;j<2;j++)
	{
	hour_char[j]=split2[j];
	}
	for( j=2;j<4;j++)
	{
	minute_char[j-2]=split2[j];
	}
	for( j=4;j<6;j++)
	{
	seconds_char[j-4]=split2[j];
	}

	pThis->year=atoi(year_char);
	pThis->month=atoi(month_char);
	pThis->date=atoi(day_char);
	pThis->hour=atoi(hour_char);
	pThis->minute=atoi(minute_char);
	pThis->second=atoi(seconds_char);
	pThis->minute=pThis->minute+30;
	if(pThis->minute>59)
	{
		pThis->minute=pThis->minute-60;
		pThis->hour=pThis->hour+6;
		if(pThis->hour>23)
		{
			pThis->hour=pThis->hour-24;
		}
	}
	else
	{
		pThis->hour=pThis->hour+5;
		if(pThis->hour>23)
		{
			pThis->hour=pThis->hour-24;
		}
	}
	for(int j=0;j<2;j++)
	{
		lattitude_degree[j]=lattitude_char[j];
	}
	for(int j=2;j<strlen(lattitude_char);j++)
	{
		lattitude_minutes[j-2]=lattitude_char[j];
	}
	lattitude_degree_int= atoi(lattitude_degree);
  lattitude_minute_float=atof(lattitude_minutes)/1.0000;
	latitude_check=((lattitude_degree_int)+(lattitude_minute_float/60));
	if(latitude_check>15 &&  latitude_check<100)
	{
		pThis->Lattitude=latitude_check;
	}
	for(int j=0;j<3;j++)
	{
		longitude_degree[j]=longitude_char[j];
	}
	for(int j=3;j<strlen(longitude_char);j++)
	{
		longitude_minutes[j-3]=longitude_char[j];
	}
	longitude_degree_int= atoi(longitude_degree);
  longitude_minute_float=atof(longitude_minutes)/1.0000;
	longitude_check=((longitude_degree_int)+(longitude_minute_float/60));
	if(longitude_check>15 &&  longitude_check<100)
	{
		pThis->Longitude=longitude_check;
	}
	pThis->No_Of_SAT=atoi(no_of_sattelites_char);
	/******************************************************/
}



/******************global variables*****************/
char imei_no_1[3];
char imei_no_2[3];
char imei_no_3[3];
char imei_no_4[3];
char imei_no_5[3];
char imei_no_6[3];
char imei_no_7[3];
char imei_no_8[3];
/**********************************************/
/**
  * @brief  READ IMEI Number
  * @param  imei_number- variable where imei number is stored
  * @retval None
  */
void read_imei_no(char* imei_number, GSM_Param_Def *pThis )
{
	int j=0;
	memset(rx_imei_no,0,50);
	char tx_imei_no[100]="AT+GSN\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_imei_no,strlen(tx_imei_no),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_imei_no,60, 500);  // Receiving data via usart
	imei_number[0]='0';
	for(j=9;j<24;j++)
	{
		imei_number[j-9]=rx_imei_no[j];
	}
	for(j=0;j<2;j++)
	{
		imei_no_1[j]=imei_number[j];
	}
	for(j=2;j<4;j++)
	{
		imei_no_2[j-2]=imei_number[j];
	}
	for(j=4;j<6;j++)
	{
		imei_no_3[j-4]=imei_number[j];
	}
	for(j=6;j<8;j++)
	{
		imei_no_4[j-6]=imei_number[j];
	}
	for(j=8;j<10;j++)
	{
		imei_no_5[j-8]=imei_number[j];
	}
	for(j=10;j<12;j++)
	{
		imei_no_6[j-10]=imei_number[j];
	}
	for(j=12;j<14;j++)
	{
		imei_no_7[j-12]=imei_number[j];
	}
	for(j=14;j<16;j++)
	{
		imei_no_8[j-14]=imei_number[j];
	}
	pThis->IMEI_hex[0]=(int)strtol(imei_no_1, NULL, 16); 
	pThis->IMEI_hex[1]=(int)strtol(imei_no_2, NULL, 16); 
	pThis->IMEI_hex[2]=(int)strtol(imei_no_3, NULL, 16); 
	pThis->IMEI_hex[3]=(int)strtol(imei_no_4, NULL, 16); 
	pThis->IMEI_hex[4]=(int)strtol(imei_no_5, NULL, 16); 
	pThis->IMEI_hex[5]=(int)strtol(imei_no_6, NULL, 16); 
	pThis->IMEI_hex[6]=(int)strtol(imei_no_7, NULL, 16); 
	pThis->IMEI_hex[7]=(int)strtol(imei_no_8, NULL, 16); 
}

char date_time_char[30];

/**
  * @brief  READ Date and time from the module
  * @param  None
  * @retval None
  */
void read_date_time(GPS_GGA_GNSS_Def *pThis)
{
	int j=0;
	memset(hour_char,0,sizeof(hour_char));
  memset(minute_char,0,sizeof(minute_char));
  memset(seconds_char,0,sizeof(seconds_char));

	memset(rx_read_date_time,0,50);
	char tx_read_date_time[100]="AT+CCLK?\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_read_date_time,strlen(tx_read_date_time),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_read_date_time,100, 500);  // Receiving data via usart
	/********************extracting the data***********************/
	if(rx_read_date_time[0]=='A' && rx_read_date_time[1]!='A')
	{
		for( j=19;j<36;j++)
		{
			date_time_char[j-19]=rx_read_date_time[j];
		}
  }
	else if(rx_read_date_time[0]=='A' && rx_read_date_time[1]=='A')
	{
		for( j=20;j<37;j++)
		{
			date_time_char[j-20]=rx_read_date_time[j];
		}
  }
	else if(rx_read_date_time[0]!='A' && rx_read_date_time[1]=='A')
	{
		for( j=20;j<37;j++)
		{
			date_time_char[j-20]=rx_read_date_time[j];
		}
  }
	for( j=3;j<5;j++)
	{
	month_char[j-3]=date_time_char[j];
	}
	for( j=6;j<8;j++)
	{
	day_char[j-6]=date_time_char[j];
	}
	for( j=9;j<11;j++)
	{
	hour_char[j-9]=date_time_char[j];
	}
	for( j=12;j<14;j++)
	{
	minute_char[j-12]=date_time_char[j];
	}
	for( j=15;j<17;j++)
	{
	seconds_char[j-15]=date_time_char[j];
	}
	for( j=0;j<2;j++)
	{
	year_char[j]=date_time_char[j];
	}
	pThis->year=atoi(year_char);
	pThis->month=atoi(month_char);
	pThis->date=atoi(day_char);
	pThis->hour=atoi(hour_char);
	pThis->minute=atoi(minute_char);
	pThis->second=atoi(seconds_char);
	/**********************************************/
}

/*********************global variables***********/
char mcc_char[10];
char mnc_char[10];
char lac_char[10];
char cellid_char[10];
/**********************************************/
/**
  * @brief  Extract MCC MNC LAC and Cell ID
  * @param  None
  * @retval None
  */
void extract_mcc_mnc_cellid_lac(GSM_Param_Def *pThis)
{
	int j=0;
	memset(rx_extract_mcc_mnc_cellid_lac,0,50);
	char tx_to_qeng_1[100]="AT+QENG=1\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_to_qeng_1,strlen(tx_to_qeng_1),100);
	HAL_Delay(200);
	char tx_extract_mcc_mnc_cellid_lac[100]="AT+QENG?\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_extract_mcc_mnc_cellid_lac,strlen(tx_extract_mcc_mnc_cellid_lac),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_extract_mcc_mnc_cellid_lac,200, 500);  // Receiving data via usart
	char *ptr1,*ptr2,*ptr3,*ptr4,*ptr5,*ptr6;
		/********************split the string based on ","*********************/
	char delim[] = ",";

	char *ptr = strtok(rx_extract_mcc_mnc_cellid_lac, delim);
    ptr1=ptr;
    ptr = strtok(NULL, delim);
    ptr2=ptr;
	ptr = strtok(NULL, delim);
    ptr3=ptr;
	ptr = strtok(NULL, delim);
    ptr4=ptr;
	    ptr = strtok(NULL, delim);
    ptr5=ptr;
		ptr = strtok(NULL, delim);
    ptr6=ptr;
		strcpy(mcc_char,ptr3);
		strcpy(mnc_char,ptr4);
		strcpy(lac_char,ptr5);
		strcpy(cellid_char,ptr6);
		pThis->Mcc=atoi(mcc_char);
		pThis->Mnc=atoi(mnc_char);
		pThis->Lac=(int)strtol(lac_char, NULL, 16); 
		pThis->CellId=(int)strtol(cellid_char, NULL, 16); 
		/*********************************************************/

}

/**********************************************/
char course_over_ground_char[50];
char speed_char[50];
char course_bool_type[10];
int course_over_ground;
/**********************************************/
/**
  * @brief  Extract VTG information
  * @param  None
  * @retval None
  */
void get_vtg_gnss_information(GPS_VGT_GNSS_Def *pThis)
{
	memset(rx_get_vtg_gnss_information,0,50);
	char tx_get_vtg_gnss_information[100]="AT+QGNSSRD=\"NMEA/VTG\"\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_get_vtg_gnss_information,strlen(tx_get_vtg_gnss_information),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_get_vtg_gnss_information,500, 700);  // Receiving data via usart
		/********************split the string based on ","*********************/  
	char *ptr1,*ptr2,*ptr3,*ptr4,*ptr5,*ptr6,*ptr7,*ptr8;
	
	char delim[] = ",";

	char *ptr = strtok(rx_get_vtg_gnss_information, delim);
    ptr1=ptr;
    ptr = strtok(NULL, delim);
    ptr2=ptr;
	ptr = strtok(NULL, delim);
    ptr3=ptr;
	ptr = strtok(NULL, delim);
    ptr4=ptr;
	    ptr = strtok(NULL, delim);
    ptr5=ptr;
	ptr = strtok(NULL, delim);
    ptr6=ptr;
	ptr = strtok(NULL, delim);
    ptr7=ptr;
		ptr = strtok(NULL, delim);
    ptr8=ptr;
		strcpy(course_over_ground_char,ptr2);
	strcpy(speed_char,ptr7);
  pThis->speed=atof(speed_char);
	course_over_ground=atoi(course_over_ground_char);
	int j=0;
	for( j=0;j<10;j++)
	{
		course_bool_type[j]=(course_over_ground>>j) & 0x01;
	} 
	for(j=0;j<8;j++)
	{
		pThis->course_status_byte_1 |= (course_bool_type[j]<<j);	
	}

	pThis->course_status_byte_2=(course_bool_type[9]<<1) | course_bool_type[8];
  if(strcpy(direction_of_lattitude,"N")==0)
	{
		pThis->course_status_byte_2|= (1<<2);
	}
	else if(strcpy(direction_of_lattitude,"S")==0)
	{
		pThis->course_status_byte_2|= (0<<2);
	}
	if(strcpy(direction_of_longitude_char,"E")==0)
	{
		pThis->course_status_byte_2|= (0<<3);
	}
	else if(strcpy(direction_of_longitude_char,"W")==0)
	{
		pThis->course_status_byte_2|= (1<<3);
	}
	pThis->course_status_byte_2|= (1<<4);
	pThis->course_status_byte_2|= (0<<5);
	pThis->course_status_byte_2|= (0<<6);
	pThis->course_status_byte_2|= (0<<7);
/*************************************************************/
}

/*************global variables******************/
uint8_t gsm_signal_start_index;
uint8_t gsm_signal_end_index;
char gsm_signal_strength_char[10];
/**********************************************/
/**
  * @brief  GET the GSM Signal Strength
  * @param  None
  * @retval None
  */
void extract_gsm_signal_strength(GSM_Param_Def *pThis)
{
	int j=0;
	memset(rx_extract_gsm_signal_strength,0,50);
	char tx_extract_gsm_signal_strength[100]="AT+CSQ\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_extract_gsm_signal_strength,strlen(tx_extract_gsm_signal_strength),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_extract_gsm_signal_strength,200, 500);  // Receiving data via usart
	for(j=0;j<strlen(rx_extract_gsm_signal_strength);j++)
	{
		if(rx_extract_gsm_signal_strength[j]==0x20)
		{
			gsm_signal_start_index=j+1;
		}
		if(rx_extract_gsm_signal_strength[j]==',')
		{
			gsm_signal_end_index=j;
		}
	}   
	for(j=gsm_signal_start_index;j<gsm_signal_end_index;j++)
	{
		gsm_signal_strength_char[j-gsm_signal_start_index]=rx_extract_gsm_signal_strength[j];
	}
	pThis->GSM_Signal_Strength=atoi(gsm_signal_strength_char);
}

