#ifndef __MC60_GPS_H
#define __MC60_GPS_H

#include "string.h"
#include "stdbool.h"
#include "stm32f1xx_hal.h"


//GPS data structure

typedef struct GPS_GGA_GNSS
{
	uint16_t year;
	uint8_t month;
	uint8_t date;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	double Lattitude;
	double Longitude;
	uint8_t No_Of_SAT;
	bool GPS_ACTIVE;
}GPS_GGA_GNSS_Def;

typedef struct GPS_VGT_GNSS
{
	char course_status_byte_1;		// not used
	char course_status_byte_2;		// not used
	uint8_t speed;	
}GPS_VGT_GNSS_Def;

typedef struct GSM_Param
{
	int Mcc;
	int Mnc;
	int Lac;
	int CellId;
	int GSM_Signal_Strength;
	char IMEI_hex[10];	
}GSM_Param_Def;

extern UART_HandleTypeDef huart1;

void read_imei_no(char* imei_number , GSM_Param_Def *pThis);       //READ IMEI Number
void read_date_time(GPS_GGA_GNSS_Def *pThis);                  //READ Date and time from the module
void set_gnss_on(void);                     //To make GNSS ON
void get_rmc_gnss_information(GPS_GGA_GNSS_Def *pThis);        //To extract RMC information
void get_gga_gnss_information(GPS_GGA_GNSS_Def *pThis);        // To extract GGA information
void get_vtg_gnss_information(GPS_VGT_GNSS_Def *pThis);        //Extract VTG information
void extract_mcc_mnc_cellid_lac(GSM_Param_Def *pThis);      //Extract MCC MNC LAC and Cell ID
void extract_gsm_signal_strength(GSM_Param_Def *pThis);     //GET the GSM Signal Strength



#endif
