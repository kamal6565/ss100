#include "MC60_GSM.h"
#include "stdio.h"

/**********global variables*************/
char rx_buff_apn[50];
char rx_set_baudrate[50];
char rx_buff_tcp[400];
char rx_buff_get_ip[50];
char rx_buff_sim_test[100];
char rx_buff_connect_gprs[50];
char rx_buff_send_packet[100];
char rx_buffcloseTCP[100];
char check_tcp_connect[20];
char rx_tcp_through_dns[100];
char length_in_char[10];
char rx_buff_tcp_transparent[50];
/***************************************/

/**
  * @brief  To set the APN 
  * @param  APN to be SET
  * @retval None
  */
void set_baudrate_9600()
{
	memset(rx_set_baudrate,0,50);
	char tx_set_baudrate[100]="AT+IPR=9600\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_set_baudrate,strlen(tx_set_baudrate),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_set_baudrate,50, 100);  // Receiving data via usart
}
/**
  * @brief  To set the APN 
  * @param  APN to be SET
  * @retval None
  */
void set_apn(char* apn)
{
	memset(rx_buff_apn,0,50);
	char tx_buff_apn[100]="AT+QICSGP=1,\"";
	strncat(tx_buff_apn,apn,strlen(apn));
	strncat(tx_buff_apn,"\"\r\n",9);
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_buff_apn,strlen(tx_buff_apn),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_buff_apn,50, 100);  // Receiving data via usart
	
}
/**
  * @brief  Start TCP/IP communication
  * @param  IP Number and port number of the server
  * @retval connection established or not(0 or 1)
  */
bool start_tcp(char* ip,char* port)
{
	char* check_connect=NULL;
	memset(rx_buff_tcp,0,sizeof(rx_buff_tcp));
	char tx_buff_tcp[100]="AT+QIOPEN=\"TCP\",\"";
	strncat(tx_buff_tcp,ip,strlen(ip));
	strncat(tx_buff_tcp,"\",",3);
	strncat(tx_buff_tcp,port,strlen(port));
	strncat(tx_buff_tcp,"\r\n",3);
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_buff_tcp,strlen(tx_buff_tcp),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_buff_tcp,100, 10000);  // Receiving data via usart
	check_connect=strstr(rx_buff_tcp,"CONNECT OK");
	if(check_connect !=NULL)
	{
		memset(check_tcp_connect,0,sizeof(check_tcp_connect));
		memset(rx_buff_tcp,0,sizeof(rx_buff_tcp));
		return 1;
	}
	else
	{
		memset(rx_buff_tcp,0,sizeof(rx_buff_tcp));
		return 0;
	}
}
/**
  * @brief  To get the modules IP 
  * @param  None
  * @retval None
  */
void get_ip()
{
	memset(rx_buff_get_ip,0,50);
	char tx_vuff_get_ip[100]="AT+CIFSR\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_vuff_get_ip,strlen(tx_vuff_get_ip),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_buff_get_ip,50, 100);  // Receiving data via usart
	
}
/**
  * @brief  Close the TCP connection 
  * @param  None
  * @retval None
  */
void closeTCP(void)
{
	memset(rx_buffcloseTCP,0,100);
	char tx_vuffcloseTCP[100]="AT+QICLOSE\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_vuffcloseTCP,strlen(tx_vuffcloseTCP),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_buffcloseTCP,50, 5000);  // Receiving data via usart
	
}
/**
  * @brief  Send the packet on TCP
  * @param  length of the packet
  * @retval None
  */
void send_packet_over_tcp(int length)
{
	memset(rx_buff_send_packet,0,100);
	char tx_buff_to_send_packet[100]="AT+QISEND=";
	sprintf(length_in_char,"%i\r\n",length);
	strncat(tx_buff_to_send_packet,length_in_char,strlen(length_in_char));
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_buff_to_send_packet,strlen(tx_buff_to_send_packet),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_buff_send_packet,500, 100);  // Receiving data via usart
}
/**
  * @brief  Check the SIM in the module
  * @param  None
  * @retval None
  */
void check_sim()
{
	memset(rx_buff_sim_test,0,100);
	char tx_vuff_sim_test[100]="AT+CGREG?\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_vuff_sim_test,strlen(tx_vuff_sim_test),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_buff_sim_test,100, 2000);  // Receiving data via usart
}
/**
  * @brief  Connect the module with GPRS
  * @param  None
  * @retval None
  */
void conect_gprs()
{
	memset(rx_buff_connect_gprs,0,50);
	char tx_vuff_connect_gprs[100]="AT+QIACT=?\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_vuff_connect_gprs,strlen(tx_vuff_connect_gprs),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_buff_connect_gprs,100, 100);  // Receiving data via usart
}
/**
  * @brief  SET thr TCP to Transparent mode
  * @param  None
  * @retval None
  */
void set_tcp_transparent()
{
	memset(rx_buff_tcp_transparent,0,50);
	char tx_vuff_tcp_transparent[100]="AT+QIMODE=0\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_vuff_tcp_transparent,strlen(tx_vuff_tcp_transparent),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_buff_tcp_transparent,100, 100);  // Receiving data via usart
}

void send_packet_through_tcp(int length,char* packet)
{
	HAL_UART_Transmit(&huart1,(uint8_t*)packet,length,100);
}
/**
  * @brief  To enable DNS
  * @param  None
  * @retval None
  */
void set_tcp_through_dns()
{
	memset(rx_tcp_through_dns,0,50);
	char tx_tcp_through_dns[100]="AT+QIDNSIP=1\r\n";
	HAL_UART_Transmit(&huart1,(uint8_t*)tx_tcp_through_dns,strlen(tx_tcp_through_dns),100);
	HAL_UART_Receive(&huart1, (uint8_t*)rx_tcp_through_dns,100, 100);  // Receiving data via usart
}

void send_mqtt_packet(int length,char* packet)
{
	HAL_UART_Transmit(&huart1,(uint8_t*)packet,length,100);
}

