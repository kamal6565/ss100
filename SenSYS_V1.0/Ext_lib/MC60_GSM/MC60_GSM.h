#ifndef __MC60_GSM_H
#define __MC60_GSM_H

#include "string.h"
#include "stdbool.h"
#include "stm32f1xx_hal.h"


extern UART_HandleTypeDef huart1;


void set_apn(char* apn);              //To set the APN 
bool start_tcp(char* ip,char* port);   //Start TCP/IP communication
void get_ip(void);                   //To get the modules IP 
void check_sim(void);            //Check the SIM in the module
void conect_gprs(void);          //Connect the module with GPRS
void send_packet_over_tcp(int length);    //Send the packet on TCP
void send_packet_through_tcp(int length,char* packet);
void closeTCP(void);             //Close the TCP connection            
void set_tcp_transparent(void);      //SET thr TCP to Transparent mode
void set_tcp_through_dns(void);      //To enable DNS
void send_mqtt_packet(int length,char* packet);
void set_baudrate_9600(void);


#endif
