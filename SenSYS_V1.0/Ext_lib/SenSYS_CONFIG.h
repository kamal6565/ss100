


/*****************************************/
//Includes
#include "stdbool.h"
#include "main.h"
/*****************************************/



/*******************************************/
//Defines

#define pi 3.14159265358979323846
#define USERNAME         "hyperx"
#define PASSWORD         "1234"
#define DEVICE_ID					1
#define Version						0 										// Manufacturer DeviceModel, PacketFormatVersion
#define PUBLISH_TOPIC   "LOCATION"              //topic for publishing packet
#define HEARTBEAT_TOPIC   "PING"
#define MQTT_PROTOCOL_NAME   "MQIsdp"           //MQIsdp-in version 3.1 & MQTT-in version 3.1.1
#define SUB_TOPIC         "CONFIG"
#define MQTT_PROTOCOL_NUMBER  3                 //3-in version 3.1 & 4-in version 3.1.1
#define MQTT_BIRTH_MSG_EN     1
#define MQTT_USERNAME_FLAG    1
#define MQTT_PASSWORD_FLAG    1
#define WILL_MSG_FLAG         0

#if MQTT_USERNAME_FLAG
#define MQTT_USERNAME     "exmkiyra"
#else
#define MQTT_USERNAME     ""
#endif

#if MQTT_PASSWORD_FLAG
#define MQTT_PASSWORD     "8lEE3l5H8ocZ"	
#else
#define MQTT_PASSWORD     ""
#endif

#if WILL_MSG_FLAG
#define WILL_MSG     "DLMS CONNECTED"
#define WILL_TOPIC   "STATUS"
#else
#define WILL_MSG     ""
#define WILL_TOPIC   ""
#endif

#if MQTT_BIRTH_MSG_EN
#define BIRTH_MSG     "SMART_SYSTEM_CONNECT"
#define BIRTH_TOPIC   "STATUS"
#endif

/******************************************/

/********************************************/
//Enums

enum
{
  FAIL = 0,
  SUCC
};

enum
{
  IDLE = 0,
  MOTION
};
/************************************************/



/**********************************************/

//structure

typedef struct 
{
 bool Previous_State;
 bool Current_State; 
}State_t;

struct Device_Batt_t
{
	float Current;               // array is being used to store dma data, check how to use it.
	float Voltage;
};




typedef struct  
{
	State_t State;
}HighSpeed_t;

/***********************************/
/*typedef struct  
{
	State state;
}Towing;
*/
typedef struct  
{
	State_t State;
}IGOffMovement_t;
/***********************************/
typedef struct  
{
	State_t State;
}HighTemp_t;

typedef struct  
{
	State_t State;
}Ignition_t;

typedef struct  
{
	State_t State;
}Movement_t;

typedef struct  
{
	State_t State;
}LOWBATT_t;

typedef struct  
{
	State_t State;
}BATTRemoved_t;

typedef struct  
{
	State_t State;
}BATTCharge_t;

struct Event_t
{
	HighSpeed_t HighSpeed;
	IGOffMovement_t IG_OFF_Movement;
	HighTemp_t HighTemp;
	Ignition_t Ignition;
	Movement_t Movement;
	LOWBATT_t LOW_BATT;
	BATTRemoved_t BATT_Removed;
	BATTCharge_t BATT_Charging;
};