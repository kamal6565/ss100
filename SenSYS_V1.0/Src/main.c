/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stdio.h"
#include "string.h"
#include "math.h"
#include "stdbool.h"
#include "stdlib.h"
#include "SenSYS_CONFIG.h"

#include "MC60_GPS.h"
#include "MC60_GSM.h"
#include "MC60_GSM_SMS.h"
#include "MQTT.h"
#include "DHT22.h"
#include "IMU_MPU_6050.h"

#define MAX_GEOFENCE_LIMIT 20
#define UART_RX_PACKET_LEN 100
/*#define deg2rad(deg) return (deg * pi / 180)  // need to test
#define rad2deg(rad) return (rad * 180 / pi)*/
double deg2rad(double deg) 
{
  return (deg * pi / 180);
}
double rad2deg(double rad) 
{
  return (rad * 180 / pi);
}
void itoa(int num, char *str)
{
   sprintf(str, "%d", num);
}

/* Local Variable **************************************************************/
uint16_t ADC_Data[2];       // Buffer to store current and voltage sensor data

char mqtt_ip[100]="tailor.cloudmqtt.com";    //ip for mqtt
char mqtt_port_number[50]="14373";               //port of mqtt
char SimAPN[40]="www";              //apn for sim

uint32_t Msg_ID = 0;
uint8_t UART_RX_Packet[UART_RX_PACKET_LEN];

bool Time_Flag=false;
bool GeoFence_Center_Flag = false;
bool UART_RX_Flag = false;
bool HB_Flag = false;
bool Event_Flag = false;
/********************************************************************************/





/********************************************************************************/



/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

I2C_HandleTypeDef hi2c1;

IWDG_HandleTypeDef hiwdg;

TIM_HandleTypeDef htim2;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart1_rx;

/* Packet variables****************************************************************************/
GPS_GGA_GNSS_Def GPS_GGA_GNSS_Data;
GPS_VGT_GNSS_Def GPS_VGT_GNSS_Data;
GSM_Param_Def GSM_Param;

MPU_ConfigTypeDef MpuConfig_Data;
ScaledData_Def AccelScaled, GyroScaled;
GPS_GGA_GNSS_Def GPS_GGA_GNSS_Param;
GPS_VGT_GNSS_Def GPS_VGT_GNSS_Param;
GSM_Param_Def GSM_Param;
DHT22_Def DHT22_Param;

struct Event_t Event;
struct Device_Batt_t Device_Batt;

struct TCP_Connection
{
	bool Status;
	uint8_t Attemp;
};
struct CV_Sensor
{
	uint16_t Current; 
	uint16_t Voltage;
};
struct GeoFence
{
	float Latitude; 
	float Longitude;
};

struct TCP_Connection TCP_Conncetion_Param;
struct CV_Sensor CV_Sensor_Param;
struct GeoFence GeoFence_Param;

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_ADC1_Init(void);
static void MX_I2C1_Init(void);
static void MX_IWDG_Init(void);
static void MX_TIM2_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);


/**DEVICE_ID Init **********************************************/
static void MC60_Init(void);
static void MC60_TCP_Init(void);
static void IMU_Init(void);
/***************************************************************/

/*Device handlers*************************************************/
static void IMU_InputHandler(void);					// fetch IMU sensor input and fill data structure
static void CV_InputHandler(void);					// convert ADC values to corresponding current and voltage notations
static void DHT_InputHandler(void);					// DHT sensor input 
static void GeoFencing_AlertHandler(void);	// Alert when device goes beyond range 
static void Time_PacketHandler(char []);
static void HeartBeat_PacketHandler(void);
static void Event_PacketHandler(void);


/****************************************************************/
// Managers

void Sensor_Manager(void);
void GPS_Manager(void);				// fetch gga nad vgt information and store
void Frame_Manager(void);			// heartbeat , event packet generator and tx ove serial mqtt
void Comm_Manager(void); 			// MQTT subscribe messages and sms responder
void LED_Manager(void); 			// handles led output
void Connection_Manager(void); 	//chech connecton status, reconnection
void Error_Manager(void); 			// check for error, alerts

/****************************************************************/

/**
  * @brief  Rx Half Transfer completed callbacks.
  * @param  huart  Pointer to a UART_HandleTypeDef structure that contains
  *                the configuration information for the specified UART module.
  * @retval None
  */
void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart)
{
  /* Prevent unused argument(s) compilation warning */
  UNUSED(huart);
  /* NOTE: This function should not be modified, when the callback is needed,
           the HAL_UART_RxHalfCpltCallback could be implemented in the user file
   */
	UART_RX_Flag = true;
}


/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  MX_IWDG_Init();
  MX_TIM2_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();

	
	HAL_ADC_Start_DMA(&hadc1,(uint32_t *)ADC_Data,2);	
	//HAL_UART_Receive_DMA(&huart2,UART_RX_Packet,UART_RX_PACKET_LEN);   used when UART RX feature is used
	MC60_Init();
	MC60_TCP_Init();
	
	
  while (1)
  {
		/************************************************/
		Sensor_Manager();
		GPS_Manager();
		Frame_Manager();					// handle heartbeat and event packet generation and transmission to server
		/**************************************************/
		// connection handler    			// connection check // reconnection
		/*if(UART_RX_Flag)
		{
			UART_RX_Flag = false;
		// UART RX handler , com manager
		}
		*/
		Comm_Manager(); 				// MQTT subscribe messages and sms responder
		//LED_Manager(); 					// handles led output
		//Connection_Manager(); 	//chech connecton status, reconnection
		//Error_Manager();
		/*************alert/error handler************************/
		GeoFencing_AlertHandler();
		// Error handler
		// LED handler
  }

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Common config 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ScanConvMode = ADC_SCAN_ENABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 2;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel 
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Regular Channel 
  */
  sConfig.Channel = ADC_CHANNEL_1;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief IWDG Initialization Function
  * @param None
  * @retval None
  */
static void MX_IWDG_Init(void)
{

  /* USER CODE BEGIN IWDG_Init 0 */

  /* USER CODE END IWDG_Init 0 */

  /* USER CODE BEGIN IWDG_Init 1 */

  /* USER CODE END IWDG_Init 1 */
  hiwdg.Instance = IWDG;
  hiwdg.Init.Prescaler = IWDG_PRESCALER_4;
  hiwdg.Init.Reload = 4095;
  if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN IWDG_Init 2 */

  /* USER CODE END IWDG_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_SlaveConfigTypeDef sSlaveConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 0;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sSlaveConfig.SlaveMode = TIM_SLAVEMODE_DISABLE;
  sSlaveConfig.InputTrigger = TIM_TS_ITR0;
  if (HAL_TIM_SlaveConfigSynchro(&htim2, &sSlaveConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  /* DMA1_Channel5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(DHT_GPIO_Port, DHT_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, DO1_Pin|DO2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : PA4 */
  GPIO_InitStruct.Pin = GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : DHT_Pin */
  GPIO_InitStruct.Pin = DHT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(DHT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : DO1_Pin DO2_Pin */
  GPIO_InitStruct.Pin = DO1_Pin|DO2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI4_IRQn);

}

/***********************************************************************/
// Managers definitions

void Sensor_Manager(void)
{
	CV_InputHandler();		// current / voltage sensor input
	IMU_InputHandler();		// IMU sensor input
	DHT_InputHandler();		// DHT handler or temperature handler
}

void GPS_Manager(void)
{
	get_rmc_gnss_information(&GPS_GGA_GNSS_Param);	// to check gps is active or not or GNSS is ON/OFF
	if(GPS_GGA_GNSS_Param.GPS_ACTIVE)
	{
		get_gga_gnss_information(&GPS_GGA_GNSS_Param);
		get_vtg_gnss_information(&GPS_VGT_GNSS_Param);
		if(!GeoFence_Center_Flag)
		{
			GeoFence_Param.Latitude = GPS_GGA_GNSS_Param.Lattitude;
			GeoFence_Param.Longitude = GPS_GGA_GNSS_Param.Longitude;
			GeoFence_Center_Flag = true;
		}
	}
}

// it will take care of packets creation and tx from device to server, server to device will be taken care by com_manager
void Frame_Manager()
{
	// Heartbeat packet
	//event based on 1sec timer
	HeartBeat_PacketHandler();
	Event_PacketHandler();
}

/**********************************************************************/

/**********************************************************************/
//Inits
static void MC60_Init(void)
{
	char imei[16];
	set_baudrate_9600();                      // to set GSM baut rate to 9600
  
	check_sim();                                   //checks if the status of the SIM
	HAL_Delay(200);
	
	set_apn(SimAPN);                              //Sets the APN of the SIM
	HAL_Delay(200);
	
	conect_gprs();                                 //connect with the network
	HAL_Delay(200);
	delete_all_messages();                         //delete all the messages in the SIM
	
	//HAL_IWDG_Refresh(&hiwdg);
	HAL_Delay(200);
  
	new_msg_indication();                          // for new message indication
	HAL_Delay(200);
 
	set_sms_text_mode();                           //to configure SMS in Text mode 
	HAL_Delay(200);
	
	set_character_set_to_gsm();                    //Set sms in GSM mode
  set_gnss_on();                                 //set on GNSS
	
	closeTCP();                                    //close the tcp connection (if any)
	set_tcp_through_dns();
	HAL_Delay(200);
	
	set_tcp_transparent();                         //Set the TCP in the transparent mode
  read_imei_no(imei, &GSM_Param);
	HAL_Delay(200);
	
	//HAL_IWDG_Refresh(&hiwdg);
}


// only for testing purpose
uint16_t length_con;
uint16_t length_pub;
char conn_buff[200];
char pub_buff[200];
char client_ID[50]="GRRENLY";                        //client id for connect packet


static void MC60_TCP_Init(void)
{
	TCP_Conncetion_Param.Status = 0;
	TCP_Conncetion_Param.Attemp = 0;
	while(!TCP_Conncetion_Param.Status)
	{
		//HAL_IWDG_Refresh(&hiwdg);
		TCP_Conncetion_Param.Status=start_tcp(mqtt_ip,mqtt_port_number);
		HAL_IWDG_Refresh(&hiwdg);
		if(TCP_Conncetion_Param.Status)           //start the TCP connection
		{
			HAL_IWDG_Refresh(&hiwdg);
			length_con=connect_packet((uint8_t*)conn_buff,MQTT_PROTOCOL_NAME,MQTT_PROTOCOL_NUMBER,client_ID, MQTT_USERNAME_FLAG, MQTT_PASSWORD_FLAG,MQTT_USERNAME,MQTT_PASSWORD, 1, WILL_MSG_FLAG, 0,0,WILL_TOPIC,WILL_MSG,20 );  //make the connect packetbased on the listed parameters like passwork username, will message,will topic
			send_packet_over_tcp(length_con);
			send_mqtt_packet(length_con,conn_buff);        //send the mqtt packet
			HAL_Delay(200);
			length_pub=publish_packet((uint8_t*)pub_buff,0,0,Msg_ID,BIRTH_TOPIC,BIRTH_MSG);  //make the publish packet based on publish topic and publish message
			send_packet_over_tcp(length_pub);
			send_mqtt_packet(length_pub,pub_buff);         //send the mqtt packet
			HAL_Delay(200);
			memset(pub_buff,0,200);                        //clearing the buffer
		}
		else
		{
			HAL_IWDG_Refresh(&hiwdg);
			check_sim();                                   //checks if the status of the SIM
			HAL_Delay(200);
			set_apn(SimAPN);                              //Sets the APN of the SIM
			HAL_Delay(200);
			conect_gprs();                                 //connect with the network
			HAL_Delay(200);						
			HAL_IWDG_Refresh(&hiwdg);
			closeTCP();
			set_tcp_transparent();                         //Set the TCP in the transparent mode
			HAL_Delay(200);
			TCP_Conncetion_Param.Attemp++;
			}
	}
}


static void IMU_Init(void)
{
	MPU6050_Init(&hi2c1);
	//2. Configure Accel and Gyro parameters
	MpuConfig_Data.Accel_Full_Scale = AFS_SEL_4g;
	MpuConfig_Data.ClockSource = Internal_8MHz;
	MpuConfig_Data.CONFIG_DLPF = DLPF_184A_188G_Hz;
	MpuConfig_Data.Gyro_Full_Scale = FS_SEL_500;
	MpuConfig_Data.Sleep_Mode_Bit = 0;  //1: sleep mode, 0: normal mode
	MPU6050_Config(&MpuConfig_Data);  /* USER CODE END 2 */
	//HAL_IWDG_Refresh(&hiwdg);
}

/*****************************************************************************/


/*****************************************************************************/
// Handlers
static void IMU_InputHandler(void)
{
	  MPU6050_Get_Accel_Scale(&AccelScaled);
		MPU6050_Get_Gyro_Scale(&GyroScaled);
}

static void CV_InputHandler(void)
{
	// current voltage calculation 
}

static void DHT_InputHandler(void)
{
	
}

double distance_geo_fence(double lat1, double lon1, double lat2, double lon2) 
{
  double theta, dist;
  if ((lat1 == lat2) && (lon1 == lon2)) 
	{
    return 0;
  }
  else 
	{
    theta = lon1 - lon2;
    dist = sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * cos(deg2rad(theta));
    dist = acos(dist);
    dist = rad2deg(dist);
    dist = dist * 60 * 1.1515;
		dist = dist * 1.609344;
    return (dist);
  }
}
static void GeoFencing_AlertHandler(void)
{
	if(distance_geo_fence(GeoFence_Param.Latitude,GeoFence_Param.Longitude,GPS_GGA_GNSS_Param.Lattitude,GPS_GGA_GNSS_Param.Longitude)>MAX_GEOFENCE_LIMIT)
	{
		//Geo fence alert , device ignition off , alert mesage to remote etc
	}
}

static void HeartBeat_PacketHandler(void)
{
	if(HB_Flag)
	{
		uint8_t len=0;
		HB_Flag = false;
		char HB_Packet[100],Time[17];
		memset(HB_Packet,0,sizeof(HB_Packet));
		memset(Time,0,sizeof(Time));
		Time_PacketHandler(Time);
		sprintf(HB_Packet,"{\"Device\":\"%d\",\"Ver\":\"%d\",\"TS\":\"%s\",\"LAT\":\"%f\",\"LON\":\"%f\"}",DEVICE_ID,Version,Time,
						GPS_GGA_GNSS_Data.Lattitude,GPS_GGA_GNSS_Data.Longitude);
		len=publish_packet((uint8_t*)pub_buff,0,0,Msg_ID,PUBLISH_TOPIC,HB_Packet);  //make the publish packet based on publish topic and publish message
		send_packet_over_tcp(len);
		send_mqtt_packet(length_pub,pub_buff);         //send the mqtt packet
		Msg_ID++;
	}
}

static void Event_PacketHandler(void)
{
	if(Event_Flag)
	{
		uint8_t len = 0;
		Event_Flag = false;
		char Event_Packet[100],Time[17];
		memset(Event_Packet,0,sizeof(Event_Packet));
		memset(Time,0,sizeof(Time));
		Time_PacketHandler(Time);
		unsigned char event = 0;
		uint8_t i = 0;
		for(i=0;i<8;i++)
		{
			switch(i)
			{
				case 0 :	event |= Event.HighSpeed.State.Current_State << i;
									break;
				case 1 : 	event |= Event.IG_OFF_Movement.State.Current_State<< i;
									break;
				case 2 : 	event |= Event.HighTemp.State.Current_State << i;
									break;
				case 3 : 	event |= Event.Ignition.State.Current_State << i;
									break;		
				case 4 : 	event |= Event.Movement.State.Current_State << i;
									break;
				case 5 : 	event |= Event.LOW_BATT.State.Current_State << i;
									break;
				case 6 : 	event |= Event.BATT_Removed.State.Current_State << i;
									break;
				case 7 :	event |= Event.BATT_Charging.State.Current_State << i;
									break;
			}
		}
		// check multiline working else move to single line
		sprintf(Event_Packet,"{\"Device\":\"%d\",\"Ver\":\"%d\",\"TS\":\"%s\",\"LAT\":\"%f\",\"LON\":\"%f\",\"Event\":\"%c\",\
						\"Temp\":\"%f\",\"Humidity\":\"%f\",\"BattV\":\"%.2f\",\"BattI\":\"%f\",\"speed\":\"%d\",\"GyroX\":\"%.2f\",\
						\"GyroY\":\"%.2f\",\"GyroZ\":\"%.2f\"}",
						DEVICE_ID,Version,Time,GPS_GGA_GNSS_Data.Lattitude,GPS_GGA_GNSS_Data.Longitude,event,DHT22_Param.Temp,
						DHT22_Param.Humidity,Device_Batt.Voltage,Device_Batt.Current,GPS_VGT_GNSS_Data.speed,GyroScaled.x,
						GyroScaled.y,GyroScaled.z);
		len=publish_packet((uint8_t*)pub_buff,0,0,Msg_ID,PUBLISH_TOPIC,Event_Packet);  //make the publish packet based on publish topic and publish message
		send_packet_over_tcp(len);
		send_mqtt_packet(length_pub,pub_buff);         //send the mqtt packet
		Msg_ID++;
	}
}

	/**
  *******************************************************************************************************************************************

  * @file    Time_PacketHandler
  * @brief   This function is responsible for creating a time string
  * @param   Time_str is string to which Time packet will be copied.

  *******************************************************************************************************************************************
**/
static void Time_PacketHandler(char Time_str[])
{

	char date[2],month[2],year[2],hour[2],min[2],sec[2];
	
	itoa(GPS_GGA_GNSS_Data.date,date);
	if(strlen(date)<2)
	{
		date[1]=date[0];
		date[0]='0';
	}
	
	itoa(GPS_GGA_GNSS_Data.month,month);
	if(strlen(month)<2)
	{
		month[1]=month[0];
		month[0]='0';
	}

	itoa(GPS_GGA_GNSS_Data.year,year);
	if(strlen(year)<2)
	{
		year[1]=year[0];
		year[0]='0';
	}
	
	itoa(GPS_GGA_GNSS_Data.hour,hour);
	if(strlen(hour)<2)
	{
		hour[1]=hour[0];
		hour[0]='0';
	}
	
	itoa(GPS_GGA_GNSS_Data.minute,min);
  if(strlen(min)<2)
	{
		min[1]=min[0];
		min[0]='0';
	}
	
	itoa(GPS_GGA_GNSS_Data.second,sec);
  if(strlen(sec)<2)
	{
		
		sec[1]=sec[0];
		sec[0]='0';
	}
	
	sprintf(Time_str,"%s:%s:%s %s:%s:%s",year,month,date,hour,min,sec);
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
